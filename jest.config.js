module.exports = {
  projects: [
    '<rootDir>/apps/e-shop',
    '<rootDir>/libs/products',
    '<rootDir>/libs/tabs',
    '<rootDir>/libs/shopping-bag',
    '<rootDir>/libs/payment',
    '<rootDir>/libs/shipping',
    '<rootDir>/libs/store',
  ],
};
