import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ToolbarModule } from '@e-shop/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductsModule } from '@e-shop/products';
import { TabsModule } from '@e-shop/tabs';
import { StoreModule } from '@ngrx/store';
import { collectionReducer, productsReducer } from '@e-shop/store';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToolbarModule,
    ProductsModule,
    TabsModule,
    StoreModule.forRoot({
      products: productsReducer,
      collection: collectionReducer,
    }),
  ],

  bootstrap: [AppComponent],
})
export class AppModule {}
