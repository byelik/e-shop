import { Component } from '@angular/core';

import { select, Store } from '@ngrx/store';
import { AppState, selectProductCollection } from '@e-shop/store';

@Component({
  selector: 'e-shop-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'e-shop';
  productsCollection$ = this.store.pipe(select(selectProductCollection));

  constructor(private store: Store<AppState>) {}
}
