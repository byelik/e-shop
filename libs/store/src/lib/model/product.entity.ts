import { ResponsiveImageEntity } from './responsive-image.entity';

export interface ProductEntity {
  id: string;
  name: string;
  responsiveData: ResponsiveImageEntity;
}
