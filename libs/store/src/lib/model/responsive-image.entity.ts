export interface ResponsiveImageEntity {
  url: string;
  urlSet?: string;
}
