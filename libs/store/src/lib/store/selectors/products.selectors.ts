import { createSelector, createFeatureSelector } from '@ngrx/store';
import { AppState } from '../state/app.state';
import { ProductEntity } from '../../model/product.entity';

export const selectProducts = createSelector<
  AppState,
  ReadonlyArray<ProductEntity>,
  ReadonlyArray<ProductEntity>
>(
  (state) => state.products,
  (products) => products
);

export const selectCollectionState =
  createFeatureSelector<AppState, ReadonlyArray<string>>('collection');

export const selectProductCollection = createSelector(
  selectProducts,
  selectCollectionState,
  (
    products: ReadonlyArray<ProductEntity>,
    collection: ReadonlyArray<string>
  ) => {
    return collection.map((id) =>
      products.find((product) => product.id === id)
    );
  }
);
