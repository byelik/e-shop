import { createAction, props } from '@ngrx/store';
import { ProductEntity } from '../../model/product.entity';

export const addProduct = createAction(
  '[Product List] Add Product',
  props<{ productId: string }>()
);

export const removeProduct = createAction(
  '[Product Collection] Remove Product',
  props<{ productId: string }>()
);

export const updateProductList = createAction(
  '[Product List/API] Update All Products',
  props<{ Products: ProductEntity[] }>()
);
