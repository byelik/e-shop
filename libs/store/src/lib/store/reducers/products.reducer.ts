import { createReducer, on } from '@ngrx/store';

import { updateProductList } from '../actions/products.actions';
import { ProductEntity } from '../../model/product.entity';
import * as faker from 'faker';
import { ResponsiveImageEntity } from '../../model/responsive-image.entity';

/**
 * Generate mock products.
 * @param {number} count Number of products to generate.
 * @returns An array of [[ProductEntity]].
 */
function generateProducts(count: number): ProductEntity[] {
  return [...Array(count).keys()].map((i) => {
    const product: ProductEntity = {
      id: String(i),
      name: faker.lorem.word(5),
      responsiveData: {
        url: faker.image.imageUrl(100, 100, 'cars', true),
        urlSet: faker.image.imageUrl(100 * 2, 100 * 2, 'cars', true) + '2x',
      } as ResponsiveImageEntity,
    };
    return product;
  });
}

export const productsInitialState: ReadonlyArray<ProductEntity> =
  generateProducts(21);

export const productsReducer = createReducer(
  productsInitialState,
  on(updateProductList, (state, { Products }) => [...Products])
);
