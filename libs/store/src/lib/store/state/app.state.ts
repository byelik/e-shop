import { ProductEntity } from '../../model/product.entity';

export interface AppState {
  products: ReadonlyArray<ProductEntity>;
  collection: ReadonlyArray<string>;
}
