import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products/products.component';
import { ProductItemModule } from './products/ui/product-item/product-item.module';

@NgModule({
  imports: [CommonModule, CommonModule, ProductItemModule],
  exports: [ProductsComponent],
  declarations: [ProductsComponent],
})
export class ProductsModule {}
