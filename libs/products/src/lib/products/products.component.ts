import { Component, ViewEncapsulation } from '@angular/core';

import { select, Store } from '@ngrx/store';

import { addProduct, AppState, selectProducts } from '@e-shop/store';

@Component({
  selector: 'e-shop-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ProductsComponent {
  products$ = this.store.pipe(select(selectProducts));

  constructor(private store: Store<AppState>) {}

  onAdd(productId: string) {
    this.store.dispatch(addProduct({ productId }));
  }
}
