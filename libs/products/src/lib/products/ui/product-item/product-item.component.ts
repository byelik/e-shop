import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProductEntity } from '@e-shop/store';

@Component({
  selector: 'e-shop-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss'],
})
export class ProductItemComponent {
  @Input() data?: ProductEntity;
  @Output() addProduct: EventEmitter<string> = new EventEmitter<string>();
}
