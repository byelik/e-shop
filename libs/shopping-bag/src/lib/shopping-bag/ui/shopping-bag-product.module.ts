import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ShoppingBagProductComponent } from './shopping-bag-product.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, ScrollingModule, FormsModule],
  exports: [ShoppingBagProductComponent],
  declarations: [ShoppingBagProductComponent],
})
export class ShoppingBagProductModule {}
