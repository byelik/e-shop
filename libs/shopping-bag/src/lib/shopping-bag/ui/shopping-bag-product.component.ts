import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProductEntity } from '@e-shop/store';

@Component({
  selector: 'e-shop-shopping-bag-product',
  templateUrl: './shopping-bag-product.component.html',
  styleUrls: ['./shopping-bag-product.component.scss'],
})
export class ShoppingBagProductComponent {
  @Input() data?: ProductEntity;
  @Output() removeAction: EventEmitter<string> = new EventEmitter<string>();
}
