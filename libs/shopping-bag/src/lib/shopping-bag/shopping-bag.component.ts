import { Component, EventEmitter, Input, Output } from '@angular/core';

import { select, Store } from '@ngrx/store';
import {
  selectProductCollection,
  removeProduct,
  AppState,
  ProductEntity,
} from '@e-shop/store';

@Component({
  selector: 'e-shop-shopping-bag',
  templateUrl: './shopping-bag.component.html',
  styleUrls: ['./shopping-bag.component.scss'],
})
export class ShoppingBagComponent {
  @Input() products: (ProductEntity | undefined)[] = [];
  @Output() removeProduct: EventEmitter<string> = new EventEmitter<string>();

  productsCollection$ = this.store.pipe(select(selectProductCollection));
  constructor(private store: Store<AppState>) {}
  onRemove(productId: string | undefined) {
    if (productId) {
      this.store.dispatch(removeProduct({ productId }));
    }
  }
}
