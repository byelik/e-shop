import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShoppingBagComponent } from './shopping-bag/shopping-bag.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ShoppingBagProductModule } from './shopping-bag/ui/shopping-bag-product.module';
import { MatButtonModule } from '@angular/material/button';
@NgModule({
  imports: [
    CommonModule,
    ScrollingModule,
    ShoppingBagProductModule,
    MatButtonModule,
  ],
  exports: [ShoppingBagComponent],
  declarations: [ShoppingBagComponent],
})
export class ShoppingBagModule {}
