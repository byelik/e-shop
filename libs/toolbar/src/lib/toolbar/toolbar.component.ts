import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'e-shop-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ToolbarComponent {
  @Input() nameOfTheApp = '';
  @Input() itemsCount = 0;

  defaultText = 'Shopping bag: ';
}
