import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'e-shop-payment',
  templateUrl: './payment.component.html',
})
export class PaymentComponent {
  lettersOnly = Validators.pattern('[a-zA-Z ]*');
  expireDate = Validators.pattern('^(0[1-9]|1[0-2])/?([0-9]{4}|[0-9]{2})$');
  numbersOnly = Validators.pattern('^[0-9]+$');
  emptyIsNotAllowed = 'Empty field is not allowed';
  maxLength = 'Maximum 3 numbers';
  onlyNumbers = 'only numbers are allowed';

  paymentForm: FormGroup = new FormGroup({
    cardOwner: new FormControl('', [Validators.required, this.lettersOnly]),
    cardNumber: new FormControl('', [Validators.required, this.numbersOnly]),
    expired: new FormControl('', [Validators.required, this.expireDate]),
    code: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]{3}$'),
    ]),
  });
  @Output() validAction: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
    this.paymentForm.statusChanges.subscribe(() => {
      this.validAction.emit(this.paymentForm.invalid);
    });
  }
}
