import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { selectProductCollection, AppState } from '@e-shop/store';

@Component({
  selector: 'e-shop-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent {
  productsCollection$ = this.store.pipe(select(selectProductCollection));
  constructor(private store: Store<AppState>) {}

  isShippingDisabled = true;
  isPaymentDisabled = true;

  setShippingValid(isShippingValid: boolean) {
    this.isShippingDisabled = !isShippingValid;
  }

  setPaymentValid(value: boolean) {
    this.isPaymentDisabled = value;
  }

  showSuccessAlert(itemsCount: number) {
    alert('You have successfully bought ' + itemsCount + ' item(s)!!!');
  }
}
