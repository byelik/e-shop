import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsComponent } from './tabs/tabs.component';
import { MatTabsModule } from '@angular/material/tabs';
import { ShoppingBagModule } from '@e-shop/shopping-bag';
import { PaymentModule } from '@e-shop/payment';
import { ShippingModule } from '@e-shop/shipping';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [
    CommonModule,
    MatTabsModule,
    ShoppingBagModule,
    ShippingModule,
    PaymentModule,
    MatStepperModule,
    MatButtonModule,
  ],
  declarations: [TabsComponent],
  exports: [TabsComponent],
})
export class TabsModule {}
