import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { combineLatest } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'e-shop-shipping',
  templateUrl: './shipping.component.html',
})
export class ShippingComponent implements OnInit {
  emptyIsNotAllowed = 'Empty field is not allowed';
  onlyLettersAllowed = 'Only letters are allowed';
  maxLength = 'Maximum 3 numbers';

  lettersOnly = Validators.pattern('[a-zA-Z ]*');

  shippingForm: FormGroup = this.createForm();
  billingForm: FormGroup = this.createForm();

  @Output() validShippingAction: EventEmitter<boolean> =
    new EventEmitter<boolean>();

  isShippingFormValid$ = this.shippingForm.statusChanges.pipe(
    startWith(false),
    map(() => this.shippingForm.valid)
  );

  isBillingFormValid$ = this.billingForm.statusChanges.pipe(
    startWith(false),
    map(() => this.billingForm.valid)
  );

  formControl = new FormControl(true);
  isBillingSame$ = this.formControl.valueChanges.pipe(startWith(true));

  isFormValid$ = combineLatest([
    this.isShippingFormValid$,
    this.isBillingSame$,
    this.isBillingFormValid$,
  ]).pipe(validateForm());

  ngOnInit() {
    this.shippingForm.addControl('check', this.formControl);
  }

  createForm(): FormGroup {
    return new FormGroup({
      firstName: new FormControl('', [this.lettersOnly]),
      lastName: new FormControl('', [this.lettersOnly]),
      addressLine1: new FormControl('', [Validators.required]),
      addressLine2: new FormControl(''),
      city: new FormControl('', [Validators.required, this.lettersOnly]),
      country: new FormControl('', [Validators.required, this.lettersOnly]),
      postalCode: new FormControl('', [Validators.pattern('^[0-9]{3}$')]),
    });
  }

  onSubmit() {
    this.validShippingAction.emit(true);
  }
}

function validateForm() {
  return map(
    ([isShippingValid, isBillingSame, isBillingValid]: boolean[]) =>
      isShippingValid && (isBillingSame || isBillingValid)
  );
}
